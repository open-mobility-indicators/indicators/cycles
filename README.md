# OpenMobility Indicator jupyter notebook Cycles 
## description : [indicator.yml FILE](https://gitlab.com/open-mobility-indicators/indicators/cycles/-/blob/main/indicator.yaml)

Calcul des cycles (ilots, pâtés de maison) du réseau routier (piéton, cyclable ou voiture) à partir de fichiers OpenStreetMap au format pbf. 

Le fichier généré `ped_network_blocks.geojson` peut être réutilisé pour calculer des indicateurs sur les ilots (population-density-from-cycles, mon-quartier-a-pied). Il ne contient que la géométrie en WGS84, sans attributs.
(l'infrastructure OMI produit ensuite des tuiles vecteur .mbtiles à partir du fichier geoJSON).

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/blob/main/README.md)